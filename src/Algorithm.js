/**
 * Algorithm dictionary
 */
class Algorithm {
  constructor (name) {
    this.name = name
  }
}

/**
 * Export
 */
module.exports = Algorithm
