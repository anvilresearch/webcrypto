/**
 * KeyAlgorithm dictionary
 */
class KeyAlgorithm {
  constructor (name) {
    this.name = name
  }
}

/**
 * Export
 */
module.exports = KeyAlgorithm
