# Web Cryptography API

Shim for WebCryptoAPI in Node.js and the browser.

## Running tests

### Nodejs

```bash
$ npm test
```

### Browser (karma)

```bash
$ npm run karma
```

## MIT License

Copyright (c) 2016 Anvil Research, Inc.
